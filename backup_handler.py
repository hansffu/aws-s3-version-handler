#!/usr/bin/env python

#----------------Configuration-----------------------
bucket_name = ''
file_prefix = ''
#----------------------------------------------------

import boto
#import time
from datetime import datetime, timedelta
print boto

class Version:

    def set_deletable(self):
        self.deletable = True
        week_ago = timedelta(weeks=-1) + datetime.now()
        month_ago = timedelta(weeks=-5) + datetime.now()
        if self.date > week_ago:
            self.deletable = False
            pass
        if self.date.day == 1:
            self.deletable = False
            pass
        if self.date > month_ago and self.date.isoweekday() == 1:
            self.deletable = False
            pass


    def __init__(self, date, version_id, filename):
        self.date = date
        self.version_id = version_id
        self.set_deletable()
        self.filename = filename

    def __cmp__(self, other):
        return cmp(self.date, other.date)


    def __str__(self):
        d = self.date.strftime("%Y-%m-%d  %H:%M:%S")
        return "%s\t%s" % (d, self.version_id)
    

    


s3 = boto.connect_s3() #keys in ~/.boto file
#s3 = S3Connection('<aws access key>', '<aws secret key>')


bucket = s3.get_bucket(bucket_name)

keys = bucket.get_all_keys()
for key in keys:
    print key

file_versions = bucket.list_versions(prefix=file_prefix)

versions = []

for version in file_versions:
    date = datetime.strptime(version.last_modified, "%Y-%m-%dT%H:%M:%S.000Z")
    version_id = version.version_id
    filename = version.name
    if version_id != "null":
        versions.append(Version(date, version_id, filename))

print "\nKeeping:\n"
for version in sorted(versions, reverse=True):
    if not version.deletable:
        print version
        pass
    
print "\n\nDeletable:\n"
for version in sorted(versions, reverse=True):
    if version.deletable:
        print "Deleting: " + str(version)
        #bucket.delete_key(version.filename, version_id = version.version_id)
